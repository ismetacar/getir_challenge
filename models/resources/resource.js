const mongoose = require('mongoose')
const uniqueValidator = require('mongoose-unique-validator')

const resourceSchema = mongoose.Schema({
    key: {
        type: String,
        required: true,
        unique: true
    },
    createdAt: {
        type: Date
    },
    counts: {
        type: Array
    },
    value: {
        type: String
    }
})

resourceSchema.plugin(uniqueValidator);
const Resource = module.exports = mongoose.model('records', resourceSchema)