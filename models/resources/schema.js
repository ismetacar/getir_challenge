const RecordSchemas = {
    filterSchema: {
        'type': 'object',
        'properties': {
            'startDate': {
                'type': 'string',
                'format': 'date'
            },
            'endDate': {
                'type': 'string',
                'format': 'date'
            },
            'minCount': {
                'type': 'integer',
                'minimum': 0
            },
            'maxCount': {
                'type': 'integer',
                'minimum': 0
            }
        },
        'required': ['startDate', 'endDate', 'minCount', 'maxCount'],
        'additionalProperties': false
    }
}

module.exports = RecordSchemas