require('dotenv/config')
const express = require('express')
const mongoose = require('mongoose')
const BodyParser = require('body-parser')
const ResourceRoutes = require("./routes/resource")

mongoose.connect(
    process.env.MONGO_URI, {useNewUrlParser: true}, () => {
        console.log('connected to db', process.env.MONGO_URI)
    }
)

const app = express()
app.use(BodyParser.json())

ResourceRoutes.registerResourceRoutes(app)

// Express Not Found Handler
app.use(function (req, res, next) {
    res.status(404).json({
        message: 'Api not found error',
        code: 'errors.apiNotFoundError',
        context: {}
    })
})

app.use(function (err, req, res, next) {
    if (err.name === 'AppError') {
        res.status(err.status)
        res.json({
            message: err.message,
            code: err.code,
            context: err.context
        })
    } else if (err.statusCode && !err.statusCode.toString().startsWith('5')) {
        res.status(err.statusCode)
        res.json({
            message: err.message,
            code: 'errors.' + err.name,
            // context: error.stack
        })
    } else {
        res.status(500)
        res.json({
            message: 'An error occurred',
            code: 'errors.internalError',
            // context: err.stack
        })
    }
})


// Run App
app.listen(process.env.PORT, () => {
    console.log(`App running on port ${process.env.PORT}`)
})


// Export app for testing
module.exports = app