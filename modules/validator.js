const Ajv = require('ajv')

const ajv = new Ajv()

const Validator = {
    validateBodyBySchema: (body, schema) => {
        let valid = ajv.validate(schema, body)
        return {
            'valid': valid,
            'ajv': ajv
        }
    }
}

module.exports = Validator