const Responser = {
    toResponse: (responseBody) => {
        return {
            'data': {
                'items': responseBody,
                'count': responseBody.length
            }
        }
    }
}

module.exports = Responser