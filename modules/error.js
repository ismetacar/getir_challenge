// Custom App Error class
function AppError({message = '', code = 'errors.internalError', status = 500, context = {}} = {}) {
    this.name = 'AppError'
    this.message = message
    this.code = code
    this.status = status
    this.context = context
    const error = new Error(this.message)
    error.name = this.name
}

AppError.prototype = Object.create(Error.prototype)
module.exports = AppError
