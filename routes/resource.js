const ResourceModel = require("../models/resources/resource")
const ResourceService = require('../services/ResourceService')
const Responser = require('../modules/responser')

const ResourceRoutes = {
    registerResourceRoutes: (app) => {
        app.post('/api/v1/records/_filter', async (req, res) => {
            let {valid, ajv} = ResourceService.validateFilterBody(req.body)
            if (!valid) {
                res.status(400).json({
                    'message': 'Provided Body invalid',
                    'code': 'errors.badRequest',
                    'context': {
                        message: ajv.errorsText(ajv.errors, {
                            dataVar: 'body'
                        }),
                        errors: ajv.errors
                    }
                })
            }
            const limit = req.query.limit || 200
            const skip = req.query.skip || 0

            let isRestResponse = req.query.isRest || false
            isRestResponse = (isRestResponse === 'true');

            const responseBody = await ResourceService.getMatchedRecords(ResourceModel, req.body, limit, skip)
            if (isRestResponse) {
                res.status(200).json(Responser.toResponse(responseBody))
            } else {
                res.status(200).json({
                    'code': 0,
                    'message': 'SUCCESS',
                    'records': responseBody
                })
            }
        })
    }
}

module.exports = ResourceRoutes