const ResourceModel = require("../models/resources/resource")
const AppError = require("../modules/error")
const Validator = require('../modules/validator')
const RecordSchemas = require('../models/resources/schema')

const ResourceService = {
    getMatchedRecords: async (resource, body, limit, skip) => {
        const pipeline = [
            {
                "$project": {
                    "totalCount": {
                        "$sum": ["$counts"]
                    },
                    "key": 1,
                    "createdAt": 1,
                    "_id": 0
                }
            },
            {
                "$match": {
                    "totalCount": {
                        "$gte": body.minCount, "$lte": body.maxCount
                    },
                    "createdAt": {
                        "$gte": new Date(body.startDate), "$lte": new Date(body.endDate)
                    }
                },
            },
            {
                "$skip": parseInt(skip)
            },
            {
                "$limit": parseInt(limit)
            }
        ]

        return await ResourceModel.aggregate(pipeline).catch(err => {
            throw new AppError({
                message: 'An error occurred while executing aggregate',
                code: 'errors.internalServerError',
                status: 500,
                context: {
                    detail: err
                }
            })
        })
    },
    validateFilterBody: (body) => {
        return Validator.validateBodyBySchema(body, RecordSchemas.filterSchema)

    }
}

module.exports = ResourceService