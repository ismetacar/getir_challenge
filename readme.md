Getir Challenge Project

Note: Mongo Database uri should be in .env

Run with npm
----
```python
npm install
npm start
````

Run with docker
----
```python
docker build -t getir .
docker run -e MONGO_URI="mongo://mongouri" -p 3000:3000 -d getir
````

Mongo and getir app will automatically run. 

Tests
```python
npm install
npm test
```