const request = require('supertest')
const app = require('../app')

describe('Book Endpoints', () => {
    it('should response have code, message and records keys and response status should be 200', async () => {
        const res = await request(app).post('/api/v1/records/_filter').send({
            startDate: "2016-01-26",
            endDate: "2018-02-02",
            minCount: 50,
            maxCount: 100
        })
        expect(res.statusCode).toEqual(200)
        expect(res.body).toHaveProperty('code')
        expect(res.body).toHaveProperty('message')
        expect(res.body).toHaveProperty('records')
    })

    it('should response have data. and data contains items and count', async () => {
        const res = await request(app).post('/api/v1/records/_filter?isRest=true').send({
            startDate: "2016-01-26",
            endDate: "2018-02-02",
            minCount: 50,
            maxCount: 100
        })
        expect(res.statusCode).toEqual(200)
        expect(res.body).toHaveProperty('data')
        expect(res.body.data).toHaveProperty('items')
        expect(res.body.data).toHaveProperty('count')
    })

    it('should response records total count between minCount and maxCount', async () => {
        const res = await request(app).post('/api/v1/records/_filter').send({
            startDate: "2016-01-26",
            endDate: "2018-02-02",
            minCount: 50,
            maxCount: 100
        })

        res.body.records.forEach((record) => {
            expect(record.totalCount).toBeGreaterThanOrEqual(50);
            expect(record.totalCount).toBeLessThanOrEqual(100);
        })

    })

    it('should response code is zero and message SUCCESS', async () => {
        const res = await request(app).post('/api/v1/records/_filter').send({
            startDate: "2016-01-26",
            endDate: "2018-02-02",
            minCount: 50,
            maxCount: 100
        })

        expect(res.body.code).toEqual(0);
        expect(res.body.message).toEqual('SUCCESS');

    })

})